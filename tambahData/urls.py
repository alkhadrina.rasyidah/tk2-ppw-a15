from django.urls import path

from . import views
from .views import *

app_name = 'tambahData'

urlpatterns = [
    path('', views.formTambah, name='formTambah'),
    path('get_tambah', ajaxData, name="get_tambah"),
    path('post_data_ajax', postDataAjax, name="post_data_ajax"),
]
