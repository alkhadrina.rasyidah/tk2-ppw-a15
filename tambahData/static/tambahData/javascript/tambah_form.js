$(document).ready(function(){
    form = $('#form');
    
    var createPost = function(){
        forms = form.serializeArray();
        $.ajax({
            url : '/tambahData/get_tambah',
            type : 'POST',
            data : forms,
            success : function(data){
                    var list_data = data.nonVerif;
                    printData();

                    form.on('submit', function(event){
                        event.preventDefault();
                        createPost();
                    });

                    var printCard = function(tambahData){
                        container = $('#tambahData');
                        container.empty();
                    
                        for (i = 0; i < list_data.length; i++){

                            card = document.createElement('tr');
                            $(container).append(card);

                            descriptionProvinsi = document.createElement('th');
                            $(descriptionProvinsi).text(list_data[i].provinsi);

                            descriptionPulau = document.createElement('th');
                            $(descriptionPulau).text(list_data[i].pulau);

                            descriptionProkes = document.createElement('th');
                            $(descriptionProkes).attr("style", "white-space: pre-line;" ).text(list_data[i].protokol);

                            descriptionAlamat = document.createElement('th');
                            descriptionAlamat1 = document.createElement('ul');
                            descriptionAlamat2 = document.createElement('li');
                            $(descriptionAlamat2).text(list_data[i].web);
                            $(descriptionAlamat1).append(descriptionAlamat2);
                            $(descriptionAlamat).append(descriptionAlamat1);

                            descriptionEmail = document.createElement('th');
                            descriptionEmail1 = document.createElement('ul');
                            descriptionEmail2 = document.createElement('li');
                            $(descriptionEmail2).text(list_data[i].email);
                            $(descriptionEmail1).append(descriptionEmail2);
                            $(descriptionEmail).append(descriptionEmail1);

                            descriptionTelpon = document.createElement('th');
                            descriptionTelpon1 = document.createElement('ul');
                            descriptionTelpon2 = document.createElement('li');
                            $(descriptionTelpon2).text(list_data[i].telpon);
                            $(descriptionTelpon1).append(descriptionTelpon2);
                            $(descriptionTelpon).append(descriptionTelpon1);

                            $(card).append(descriptionProvinsi, descriptionPulau, descriptionProkes, descriptionAlamat, descriptionEmail, descriptionTelpon);

                        }
                    };

                    var printData= function(){
                        $.ajax({
                            url: '/tambahData/get_tambah',
                            data:{},
                            dataType: 'json',
                            success: function(response){
                                printCard(response['nonVer']);
                            }
                        });
                    };

            },

        printData();
            
        });
    };

});



