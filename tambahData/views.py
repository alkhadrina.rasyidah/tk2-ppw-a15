from django.shortcuts import render, redirect
from main.models import Pulau, Provinsi, Info, WebsitePemda, TelponPemda, EmailPemda, VerifiedInfo, NonVerifiedInfo
from .forms import tambahDataForm
from datetime import datetime
from django.http import JsonResponse
from rest_framework import serializers
from .forms import *

from .forms import tambahDataForm

from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='autentikasi:login')
def formTambah(request):
    infos = NonVerifiedInfo.objects.all()

    form = tambahDataForm(request.POST or None)


    if request.method == "POST":
        if form.is_valid():

            tanggal = datetime.today()
            nama_provinsi=request.POST['nama_provinsi']
            nama_pulau=request.POST['nama_pulau']
            info_protokol=request.POST['info_protokol_kesehatan']
            alamat_web=request.POST['Alamat_Web']
            email_pemda=request.POST['Email_Pemda']
            nomor_telpon=request.POST['Nomor_Telpon']
            email_user=request.POST['Email_User']
            
            pulau = Pulau.objects.get(nama = nama_pulau)
            provinsi = Provinsi.objects.create(nama = nama_provinsi, nama_pulau = pulau)
            info = Info.objects.create(tanggal = tanggal, nama_provinsi = provinsi, protokol_kesehatan = info_protokol)
            website = WebsitePemda.objects.create(info_id = info, alamat = alamat_web)
            no_telp = TelponPemda.objects.create(info_id = info, nomor_telpon = nomor_telpon)
            pemda = EmailPemda.objects.create(info_id = info, email = email_pemda)
            non_verified = NonVerifiedInfo.objects.create(info_id = info, email = email_user)

            form = tambahDataForm()
            return redirect('tambahData:formTambah')
            
        
    context = {
        'form' : form,
        'infos' : infos,
    }
    return render(request, 'tambahData/formTambah.html', context)

class nonVerifSerializer(serializers.ModelSerializer):
    pulau = serializers.CharField(read_only=True, source="info_id.nama_provinsi.nama_pulau.nama")
    provinsi = serializers.CharField(read_only=True, source="info_id.nama_provinsi.nama")
    protokol = serializers.CharField(read_only=True, source="info_id.protokol_kesehatan")

    class Meta:
        model = NonVerifiedInfo
        fields = ['pulau','provinsi','protokol']


def ajaxData(request):
    nonVer = NonVerifiedInfo.objects.all().select_related('info_id')
    serialize = NonVerifSerializer(nonVer, many=True).data
    website = [i.info_id.website_pemda.all()[0].alamat for i in nonVer]
    email = [i.info_id.email_pemda.all()[0].email for i in nonVer]
    telpon = [i.info_id.telpon_pemda.all()[0].nomor_telpon for i in nonVer]
    for i in range(len(serialize)):
        serialize[i].update({'web':website[i]})
        serialize[i].update({'email':email[i]})
        serialize[i].update({'telpon':telpon[i]})

    data={
        'nonVerif':serialize,
    }
    return JsonResponse(data)

def postDataAjax(request):
	json = {'is_posted' : False}

	if(request.method == 'POST'):
		form = tambahDataForm(data=request.POST)
		if(form.is_valid()):
			form.save()
			json['is_posted'] = True

	return JsonResponse(json)

