from django import forms
from django.forms import ModelForm
# from main.models import Pulau, Provinsi, Info, WebsitePemda, TelponPemda, EmailPemda,
from datetime import datetime, date



class tambahDataForm(forms.Form):

    Pilihan =( 
    ("Sumatera","Sumatera"), 
    ("Jawa", "Jawa"), 
    ("Kalimantan", "Kalimantan"), 
    ("Sulawesi", "Sulawesi"), 
    ("Papua", "Papua"), 
) 
    nama_pulau = forms.ChoiceField(
        choices = Pilihan
    )

    nama_provinsi = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukan nama provinsi'
            }
        )
    )

    info_protokol_kesehatan = forms.CharField(
        max_length=200,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukan Info protokol dan syarat berkunjung suatu provinsi'
            }
        )
    )

    Alamat_Web = forms.URLField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukan alamat website'
            }
        )
    )

    Email_Pemda = forms.EmailField(
        max_length=35,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukan email pemda'
            }
        )
    )

    Nomor_Telpon = forms.CharField(
        max_length=15,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukan nomor telpon/WA pemda'
            }
        )
    )

    Email_User = forms.EmailField(
        max_length=35,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukan email pemda'
            }
        )
    )












# class tambahDataModelForm(forms.ModelForm):
#     class Meta :
#         model = pulau, 
#         fields = [
#             'Pulau',
#             'Provinsi',
#             'Info_protokol_kesehatan',  
#             'alamat_website',
#             'nomor_telpon',
#             'Email',
#             'verified_id',
#             'non_verified_id'  
#         ]

#         widgets = {
#             'Pulau':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan nama pulau'}),
#             'Provinsi':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan waktu provinsi'}),
#             'Info_protokol_kesehatan':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan detail protokol kesehatan'}),
#             'alamat_website':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan alamat website '}),
#             'nomor_telpon':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan nomor telpon/WA'}),
#             'Email':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan email pemda'}),
#         }
