from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm

from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.

from .models import *
from .forms import CreateUserForm

def loginPage(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is None:
            User = get_user_model()
            user_queryset = User.objects.all().filter(email__iexact=username)
            if user_queryset:
                username = user_queryset[0].username
                user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            request.session['username'] = request.user.username
            return redirect('main:home')
        else:
            messages.info(request, 'Username OR password is incorrect')

    response = {}
    return render(request, 'autentikasi/login.html', response)

@login_required(login_url='autentikasi:login')
def logoutUser(request):
    logout(request)
    return redirect('/')

def registerPage(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + user)

            return redirect('autentikasi:login')

    response = {'form':form}
    return render(request, 'autentikasi/register.html', response)

def autentikasi(request):
    response = {}
    return render(request, 'main/home.html', response)