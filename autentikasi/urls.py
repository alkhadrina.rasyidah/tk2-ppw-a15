from django.urls import path
from . import views

app_name = 'autentikasi'

urlpatterns = [
    path('login/', views.loginPage, name='login'),
    path('register/', views.registerPage, name='register'),
    path('logout/', views.logoutUser, name="logout"),
    path('', views.autentikasi, name='autentikasi'),
]