from django.shortcuts import render
from newsapi.newsapi_client import NewsApiClient
from django.shortcuts import redirect
from django.http import JsonResponse
from rest_framework import serializers
from django.contrib.auth.models import User
from django.db.models import Q

from .forms import *
from .models import *

# Create your views here.
def home(request):
	response = {}
	return render(request, 'berita/home.html',response)

def ajaxHeadlines(request):
	try:
		newsapi = NewsApiClient(api_key='6fbbdcf18b614bccb89f053575af762b')
		news = newsapi.get_top_headlines(q='covid', country='id')
		articles = news['articles']
	except:
		articles = []

	data = {'articles': articles}
	return JsonResponse(data)

# Serialize for articles
class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = '__all__'

class ArticleSerializer(serializers.ModelSerializer):
	user = UserSerializer(read_only=True)
	short_post = serializers.ReadOnlyField(source='get_short_post')

	class Meta:
		model = Article
		fields = '__all__'

def ajaxArticles(request):
	articles = Article.objects.all()
	articlesSerialized = ArticleSerializer(articles, many=True).data

	data = {
		'articles': articlesSerialized,
		'username' : request.user.username
	}
	return JsonResponse(data)

def search(request, query):
	articles = Article.objects.filter(
		Q(title__icontains=query) | Q(post__icontains=query) | Q(user__first_name__icontains=query) | Q(date__icontains=query)
	)
	articlesSerialized = ArticleSerializer(articles, many=True).data

	data = {
		'articles': articlesSerialized,
		'username' : request.user.username
	}
	return JsonResponse(data)

def articleForm(request):
	if(request.method == 'POST'):
		form = ArticleForm(request.POST)
		if(form.is_valid()):
			form.save()
			return redirect('/berita')
	else:
		form = ArticleForm(initial={'user' : request.user})

	response = {
		'input_form' : form,
		'is_created' : True
	}

	return render(request, 'berita/articleForm.html', response)

def postArticleAjax(request):
	json = {'is_posted' : False}

	if(request.method == 'POST'):
		form = ArticleForm(data=request.POST)
		if(form.is_valid()):
			form.save()
			json['is_posted'] = True

	return JsonResponse(json)

def articleDetail(request, article_id):
	article = Article.objects.get(article_id=article_id)

	response = {
		'article' : article
	}

	return render(request, 'berita/detail.html', response)

def editArticle(request, article_id):
	article = Article.objects.get(article_id=article_id)
	if(request.method == 'POST'):
		form = ArticleForm(request.POST, instance=article)
		if(form.is_valid()):
			form.save()
			return redirect('/berita')
	else:
		form = ArticleForm(instance=article)

	response = {
		'input_form' : form,
		'is_create' : False
	}

	return render(request, 'berita/articleForm.html', response)

def deleteArticle(request, article_id):
	article = Article.objects.get(article_id=article_id)
	article.delete()

	return redirect('/berita')

def deleteArticleAjax(request, article_id):
	article = Article.objects.get(article_id=article_id)
	article.delete()

	json = {'is_deleted' : True}
	return JsonResponse(json)