from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from django.contrib.auth import login, authenticate, logout

class beritatest(TestCase):

	def test_home_urls_exist(self):
		response = Client().get('/berita/')
		self.assertEqual(response.status_code, 200)

	def test_home_render_right(self):
		response= Client().get('/berita/')
		html_response = response.content.decode('utf8')
		self.assertIn('Berita', html_response)
		self.assertIn('Artikel', html_response)
		self.assertIn('Login supaya bisa menulis artikel', html_response)

	def test_article_model(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		numModels = Article.objects.all().count()
		self.assertEqual(1, numModels)

	def test_create_urls_exist(self):
		response = Client().get('/berita/create/')
		self.assertEqual(response.status_code, 200)

	def test_detail_urls_exist(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		response = Client().get('/berita/detail/' + str(article.article_id) + '/')
		self.assertEqual(response.status_code, 200)

	def test_edit_urls_exist(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		response = Client().get('/berita/edit/' + str(article.article_id) + '/')
		self.assertEqual(response.status_code, 200)

	def test_delete_urls_exist(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		response = Client().get('/berita/delete/' + str(article.article_id), follow=True)
		self.assertEqual(response.status_code, 200)

	def test_delete_ajax_urls_exist(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		response = Client().get('/berita/delete_ajax/' + str(article.article_id), follow=True)
		self.assertEqual(response.status_code, 200)
	
	def test_get_headlines_urls_exist(self):
		response = Client().get('/berita/get_headlines')
		self.assertEqual(response.status_code, 200)

	def test_get_articles_urls_exist(self):
		response = Client().get('/berita/get_articles')
		self.assertEqual(response.status_code, 200)

	def test_post_articles_ajax_urls_exist(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')

		response = Client().post('/berita/post_article_ajax', data={'title': 'Halo2', 'post': 'Iya beb', 'user': user.id})
		self.assertEqual(response.status_code, 200)

	def test_search_urls_exist(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)

		response = Client().get('/berita/search/judul/')
		self.assertEqual(response.status_code, 200)

	def test_home_template_right(self):
		response = Client().get('/berita/')
		self.assertTemplateUsed(response, 'berita/home.html')

	def test_create_template_right(self):
		response = Client().get('/berita/create/')
		self.assertTemplateUsed(response, 'berita/articleForm.html')
	
	def test_detail_template_right(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		response = Client().get('/berita/detail/' + str(article.article_id) + '/')
		self.assertTemplateUsed(response, 'berita/detail.html')

	def test_edit_template_right(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		response = Client().get('/berita/edit/' + str(article.article_id) + '/')
		self.assertTemplateUsed(response, 'berita/articleForm.html')

	def test_post_article_working(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')

		response = Client().post('/berita/create/', data={'title': 'Halo2', 'post': 'Iya beb', 'user': user.id}, follow=True)
		self.assertEqual(response.status_code, 200)

	def test_post_edit_article_working(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		idArticle  = str(article.article_id )
		response = Client().post('/berita/edit/' + idArticle + "/", data={'title': 'Halo2', 'post': 'Hehhh', 'user': user.id}, follow=True)
		self.assertEqual(response.status_code, 200)

	def test_form_member_working(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		form_data = {
			'user': user,
			'title' : 'Judul',
			'post' : 'Baris1\nBaris2'
		}
		form = ArticleForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_home_login_template(self):
		c = Client()

		user = User.objects.create_user(username='user')
		user.set_password('password')
		user.first_name = 'User'
		user.last_name = 'New'
		user.save()

		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		
		response= c.login(username='user', password='password')

		response= c.get('/berita/')
		html_response = response.content.decode('utf8')
		self.assertIn('Tulis', html_response)

	def test_delete_post(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		article = Article.objects.create(
			user=user,
			title='Judul',
			post='Baris1\nBaris2'
		)
		response= Client().get('/berita/delete/' + str(article.article_id))

		response= Client().get('/berita/')
		html_response = response.content.decode('utf8')

		count = Article.objects.all().count()

		self.assertEqual(count, 0)