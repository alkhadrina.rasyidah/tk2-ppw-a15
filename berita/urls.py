from django.urls import path

from .views import *

app_name = 'berita'

urlpatterns = [
    path('', home, name='home'),
    path('create/', articleForm, name='create'),
    path('detail/<int:article_id>/', articleDetail, name='detail'),
    path('edit/<int:article_id>/', editArticle, name='edit'),
    path('delete/<int:article_id>', deleteArticle, name='delete'),
    path('delete_ajax/<int:article_id>', deleteArticleAjax, name='delete_ajax'),
    path('get_headlines', ajaxHeadlines, name="get_headlines"),
    path('get_articles', ajaxArticles, name="get_articles"),
    path('post_article_ajax', postArticleAjax, name="post_article_ajax"),
    path('search/<str:query>/', search, name='search')
]