$(document).ready(function(){
    form = $('#article-form');
    message = $('#message');
    
    var createPost = function(){
        forms = form.serializeArray();
        $.ajax({
            url : '/berita/post_article_ajax',
            type : 'POST',
            data : forms,
            success : function(response){
                if(response['is_posted']){
                    message.text('Artikel "' + forms[1]['value'] + '" berhasil dikirim');
                    $('#id_title').val('');
                    $('#id_post').val('');
                    printArticle();
                }else{
                    message.text('Artikel "' + forms[1]['value'] + '" gagal dikirim');
                }
            },
            error : function(xhr,errmsg,err){
                message.text(errmsg);
            }
        });
    };
    
    form.on('submit', function(event){
        event.preventDefault();
        createPost();
    });

    var printCardA = function(articles, username){
        container = $('#articles');
        container.empty();
    
        for(article of articles){
            if(article['user']['username'] != username){
                continue;
            }

            id = article['article_id'];

            card = document.createElement('div');
            $(card).addClass('card my-5 border-custom');

            body = document.createElement('div');
            $(body).addClass('card-body p-5');
            $(card).append(body);

            title = document.createElement('h3');
            $(title).addClass('card-title').text(article['title']);
            $(body).append(title);

            editContainer = document.createElement('div');
            $(editContainer).addClass('article-edit-container');

            deleteLink = document.createElement('a');
            $(deleteLink).click(function(){
                deleteArticle(id);
            });
            deleteButton = document.createElement('button');
            $(deleteButton).addClass('btn btn-danger').text('Hapus Artikel');
            $(deleteLink).append(deleteButton);

            editLink = document.createElement('a');
            $(editLink).attr('href', '/berita/edit/' + id + "/");
            editButton = document.createElement('button');
            $(editButton).addClass('btn btn-danger').text('Edit Artikel');
            $(editLink).append(editButton);

            $(editContainer).append(deleteLink,editLink);
            $(body).append(editContainer);

            descriptionContainer = document.createElement('div');
            $(descriptionContainer).addClass('card my-3 border-custom2');
            descriptionCard = document.createElement('div');
            $(descriptionCard).addClass('card-body child-card');
            description = document.createElement('p');
            $(description).addClass('card-text').text(article['short_post']);
            $(descriptionCard).append(description);
            $(descriptionContainer).append(descriptionCard);
            $(body).append(descriptionContainer);

            detail = document.createElement('p');
            $(detail).addClass('float-right')
            link = document.createElement('a');
            $(link).attr({
                'href' : "/berita/detail/" + id + "/",
                'target' : "_blank",
            }).addClass('card-link').text('Klik ini untuk baca lengkapnya..');
            $(detail).append(link);

            date = document.createElement('p');
            $(date).addClass('float-right').text(article['date']);

            $(body).append(detail, "<br>", date);

            container.append(card);
        }
    };

    var printArticle = function(){
        $.ajax({
            url: '/berita/get_articles',
            data:{},
            dataType: 'json',
            success: function(response){
                printCardA(response['articles'], response['username']);
            }
        });
    };

    var deleteArticle = function(id){
        $.ajax({
            url: '/berita/delete_ajax/' + id,
            data:{},
            dataType: 'json',
            success: function(response){
                printArticle();
            }
        });
    };

    printArticle();
});