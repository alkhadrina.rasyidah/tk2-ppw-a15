$(document).ready(function(){
    var printCardH = function(articles){
        container = $('#headlines');
        container.empty();

        for(article of articles){
            card = document.createElement('div');
            $(card).addClass('card my-5 border-custom');

            body = document.createElement('div');
            $(body).addClass('card-body p-5');
            $(card).append(body);

            title = document.createElement('h3');
            $(title).addClass('card-title').text(article['title']);
            imageContainer = document.createElement('div');
            $(imageContainer).addClass('content-image');
            image = document.createElement('img');
            $(image).addClass('img-fluid image-custom').attr('src', article['urlToImage']);
            $(imageContainer).append(image);
            $(body).append(title, imageContainer);

            descriptionContainer = document.createElement('div');
            $(descriptionContainer).addClass('card my-3 border-custom2');
            descriptionCard = document.createElement('div');
            $(descriptionCard).addClass('card-body child-card');
            description = document.createElement('p');
            $(description).addClass('card-text').text(article['description']);
            $(descriptionCard).append(description);
            $(descriptionContainer).append(descriptionCard);
            $(body).append(descriptionContainer);

            $(body).append("<p class='float-right'>Baca selengkapnya di <a href='" + article['url'] + "' target='_blank' class='card-link'>" + article['source']['name'] + "</a></p>")

            container.append(card);
        }
    };

    var printHeadline = function(){
        $.ajax({
            url: '/berita/get_headlines',
            data:{},
            dataType: 'json',
            success: function(response){
                printCardH(response['articles']);
            }
        });
    };

    var printCardA = function(articles, username){
        container = $('#articles');
        container.empty();
    
        for(article of articles){
            id = article['article_id'];

            card = document.createElement('div');
            $(card).addClass('card my-5 border-custom');

            body = document.createElement('div');
            $(body).addClass('card-body p-5');
            $(card).append(body);

            title = document.createElement('h3');
            $(title).addClass('card-title').text(article['title']);
            $(body).append(title);
            
            if(article['user']['username'] == username){
                editContainer = document.createElement('div');
                $(editContainer).addClass('article-edit-container');

                deleteLink = document.createElement('a');
                $(deleteLink).click(function(){
                    deleteArticle(id);
                });
                deleteButton = document.createElement('button');
                $(deleteButton).addClass('btn btn-danger').text('Hapus Artikel');
                $(deleteLink).append(deleteButton);

                editLink = document.createElement('a');
                $(editLink).attr('href', '/berita/edit/' + id + "/");
                editButton = document.createElement('button');
                $(editButton).addClass('btn btn-danger').text('Edit Artikel');
                $(editLink).append(editButton);

                $(editContainer).append(deleteLink,editLink);
                $(body).append(editContainer);
            }

            descriptionContainer = document.createElement('div');
            $(descriptionContainer).addClass('card my-3 border-custom2');
            descriptionCard = document.createElement('div');
            $(descriptionCard).addClass('card-body child-card');
            description = document.createElement('p');
            $(description).addClass('card-text').text(article['short_post']);
            $(descriptionCard).append(description);
            $(descriptionContainer).append(descriptionCard);
            $(body).append(descriptionContainer);

            detail = document.createElement('p');
            $(detail).addClass('float-right')
            link = document.createElement('a');
            $(link).attr({
                'href' : "/berita/detail/" + id + "/",
                'target' : "_blank",
            }).addClass('card-link').text('Klik ini untuk baca lengkapnya..');
            $(detail).append(link);

            author = document.createElement('p');
            $(author).addClass('float-right').text('Ditulis oleh ' + article['user']['first_name']);

            date = document.createElement('p');
            $(date).addClass('float-right').text(article['date']);

            $(body).append(detail, "<br>", author, "<br>", date);

            container.append(card);
        }
    };

    var printArticle = function(){
        $.ajax({
            url: '/berita/get_articles',
            data:{},
            dataType: 'json',
            success: function(response){
                printCardA(response['articles'], response['username']);
            }
        });
    };

    var deleteArticle = function(id){
        $.ajax({
            url: '/berita/delete_ajax/' + id,
            data:{},
            dataType: 'json',
            success: function(response){
                printArticle()
            }
        });
    };

    printHeadline();
    printArticle();

    info = $('#info');
    searchBox = $('#searchbox');

    var search = function(query){
        $.ajax({
            url: '/berita/search/' + query + "/",
            data:{},
            dataType: 'json',
            success: function(response){
                article = response['articles'];
                username = response['username'];

                if(article.length > 0){
                    info.text("");
                    $('#headlines-caption').slideUp();
                    $('#headlines').slideUp();
                    printCardA(article, username);
                }else{
                    info.text("Hasil tidak ditemukan");
                    $('#headlines-caption').slideDown();
                    $('#headlines').slideDown();
                    printArticle();
                }
            }
        });
    }

    searchBox.keyup(function(){
        var query = $(this).val();
        search(query);
    });
});