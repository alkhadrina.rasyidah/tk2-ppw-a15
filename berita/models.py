from django.db import models
from django.contrib.auth.models import User
from tinymce.models import HTMLField
# Create your models here.

class Article(models.Model):
    article_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE,null=False, related_name='articles')
    date = models.DateField(auto_now=True)
    title = models.CharField(max_length=50, null=False)
    post = models.TextField(null=False)

    def get_short_post(self):
        return self.post[:100]