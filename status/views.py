from django.shortcuts import render
from django.http import HttpResponseRedirect
from status.forms import PulauForm
from main.models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from rest_framework import serializers
from django.shortcuts import redirect
from collections import OrderedDict
import requests
import json
# Create your views here.


def home(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PulauForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return redirect('/status/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = PulauForm()
    
    response ={
        'form':form
    }
    return render(request, 'status/home.html',response)

class VerifSerializer(serializers.ModelSerializer):
    pulau = serializers.CharField(read_only=True, source="info_id.nama_provinsi.nama_pulau.nama")
    provinsi = serializers.CharField(read_only=True, source="info_id.nama_provinsi.nama")
    protokol = serializers.CharField(read_only=True, source="info_id.protokol_kesehatan")

    class Meta:
        model = VerifiedInfo
        fields = ['pulau','provinsi','protokol']

class ProvSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provinsi
        fields = ['nama']

def detail(request,pulau):
    ver = VerifiedInfo.objects.filter(info_id__nama_provinsi__nama_pulau__nama=pulau).select_related('info_id')
    serialize = VerifSerializer(ver, many=True).data
    website = [i.info_id.website_pemda.all()[0].alamat for i in ver]
    email = [i.info_id.email_pemda.all()[0].email for i in ver]
    telpon = [i.info_id.telpon_pemda.all()[0].nomor_telpon for i in ver]
    for i in range(len(serialize)):
        serialize[i].update({'web':website[i]})
        serialize[i].update({'email':email[i]})
        serialize[i].update({'telpon':telpon[i]})

    l =[]
    for info in range(len(serialize)):
        l.append(serialize[info]['provinsi'])
    
    nama_pulau=pulau
    
    verp = Provinsi.objects.filter(nama_pulau=pulau)

    l2 = ProvSerializer(verp, many=True).data
    
    data={
        'pulau': pulau,
        'verif':serialize,
        'no_info': l2
    }
    return JsonResponse(data)
