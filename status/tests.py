from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from .apps import *
from .views import *
from main.models import *
from datetime import datetime

class StatusTest(TestCase):

    def test_status_urls_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)
    
    def test_status_using_home_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response , 'status/home.html')

    def test_form_status_using_home_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func , home)

    def test_form_status_can_be_use(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        response = Client().post('/status/',{'nama_pulau':'Jawa'})

    def test_detail_urls_exist(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        obj2 = Provinsi.objects.create(nama='Jawa Barat',nama_pulau=obj1)
        obj3 = Info.objects.create(tanggal=datetime.today(),nama_provinsi=obj2,protokol_kesehatan="masker")
        obj4 = WebsitePemda.objects.create(info_id=obj3,alamat="https://jabarprov.go.id/")
        obj5 = EmailPemda.objects.create(info_id=obj3,email="test@gmail.com")
        obj6 = TelponPemda.objects.create(info_id=obj3,nomor_telpon="8577")
        obj7 = VerifiedInfo.objects.create(info_id=obj3)
        response = Client().get('/status/Jawa/')
        self.assertEqual(response.status_code, 200)

    def test_detail_using_detail_func(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        found = resolve('/status/{pulau}/'.format(pulau='Jawa'))
        self.assertEqual(found.func , detail)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(StatusConfig.name, 'status')
		self.assertEqual(apps.get_app_config('status').name, 'status')