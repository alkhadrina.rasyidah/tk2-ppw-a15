from django.urls import path

from . import views

urlpatterns = [
    path('', views.pencarian, name='pencarian'),
    path('getInfo/<str:query>', views.get_all_info, name="get_all_info")
]