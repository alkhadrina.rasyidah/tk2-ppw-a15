$(document).ready(function () {
    $("#accordian li h3").click(function () {
        var $parent = $(this).parent();
        if ($parent.hasClass('active')) return;
        //slide up all the link lists
        $("#accordian ul .row").not().slideUp();
        $(this).next().slideDown(function () {
            $parent.addClass('active').siblings().removeClass('active');
        });
    })
})
