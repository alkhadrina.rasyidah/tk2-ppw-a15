form = $('#form');
$(document).ready(function() {
    $(document).ready(function(){
        $("#theme").click(function(){
          $("body").css("background", "#c8ffe6");
          $("div.card.my-5.border-custom").css("background", "#dcffef");
        });
    });

    $("#accordian").click(function () {
        var $parent = $(this).parent();
        if ($parent.hasClass('active')) return;
        
        $(".row").not().slideUp();
        $(this).next().slideDown(function () {
            $parent.addClass('active').siblings().removeClass('active');
        });
    })

    function autocomplete(inp, arr) {
        var currentFocus;
       
        inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value;
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            this.parentNode.appendChild(a);
            
            for (i = 0; i < arr.length; i++) {
              if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                b = document.createElement("DIV");
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                b.addEventListener("click", function(e) {
                    inp.value = this.getElementsByTagName("input")[0].value;
                    closeAllLists();
                });
                a.appendChild(b);
              }
            }
        });
        
        inp.addEventListener("keydown", function(e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
            } else if (e.keyCode == 38) { 
            currentFocus--;
            addActive(x);
            } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
            }
        });
    
        function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
            x[i].parentNode.removeChild(x[i]);
            }
        }
        }
        
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }
    
    var provinsi = [
        "DKI Jakarta", "Jawa Barat", "Jawa Tengah", "Jawa Timur", "Yogyakarta",
        "Banten", "Aceh", "Sumatra Utara", "Sumatra Barat", "Riau", "Kepulauan Riau",
        "Bandar Lampung", "Sumatra Selatan", "Bengkulu", "Jambi", "Bangka Belitung",
        "Kalimantan Timur", "Kalimantan Utara", "Kalimantan Selatan", "Kalimantan Barat",
        "Kalimantan Tengah", "Maluku", "Maluku Utara", "Sulawesi Tenggara", "Sulawesi Selatan",
        "Papua", "Papua Barat", "Bali", "Nusa Tenggara Timur", "Nusa Tenggara Barat",
    ];

    autocomplete(document.getElementById("query"), provinsi);

    $('#reset').click(function(){
        $('#form')[0].reset();
    });

    form.on('submit', function(event) {
        event.preventDefault();
        var forms = form.serializeArray();

        $.ajax({
            url: 'getInfo/' + forms[1]['value'],forms,
            type: 'POST',
            data: {'csrfmiddlewaretoken':forms[0]['value'],forms},
            success: function(data) {

                var list = data.verified;
                $('#result').empty();
                
                if (list.length != 0) {
                    for (i = 0; i < list.length; i++) {
                        alert("Data " + forms[1]['value'] + " berhasil ditemukan!");
                        
                        var provinsi = list[i].provinsi;
                        var protokol = list[i].protokol;
                        var website = list[i].website;
                        var email = list[i].email;
                        var telpon = list[i].telpon;
                        
                        if (isAuthenticated) {
                            $('#result').append(
                                '<h1 style="text-align: center;"> Hasil Pencarian </h1>' + 
                                '<br><br>' +
                                '<h2 style="text-align: center;">' + provinsi + '</h2>' +
                                '<br></br>' +
                                '<div id="accordian">' +
                                    '<ul>' +
                                
                                        '<li class="active">' +
                                                '<h3><span></span>Protokol Kesehatan</h3>' +
                                                '<div class="row">' +
                                                    '<h4>' + protokol + '</h4>' +
                                                '</div>' +
                                        '</li>' +

                                        '<li class="active">' +
                                                '<h3><span></span>Website</h3>' +
                                                '<div class="row">' +
                                                    '<h4><a href="' + website + '" target="_blank">' + website + '</a></h4>' +
                                                '</div>' +
                                        '</li>' +

                                        '<li class="active">' +
                                                '<h3><span></span>Alamat Email</h3>' +
                                                '<div class="row">' +
                                                    '<h4>' + email + '</h4>' +
                                                '</div>' +
                                        '</li>' +

                                        '<li class="active">' +
                                                '<h3><span></span>Nomor Telpon</h3>' +
                                                '<div class="row">' +
                                                    '<h4>' + telpon + '</h4>' +
                                                '</div>' +
                                        '</li>' +

                                    '</ul>' +
                                '</div>'
                            );
                        }

                        else {
                            $('#result').append(
                                '<h1 style="text-align: center;"> Hasil Pencarian </h1>' + 
                                '<br><br>' +
                                '<h2 style="text-align: center;">' + provinsi + '</h2>' +
                                '<br></br>' +
                                '<div id="accordian">' +
                                    '<ul>' +
                                
                                        '<li class="active">' +
                                                '<h3><span></span>Protokol Kesehatan</h3>' +
                                                '<div class="row">' +
                                                    '<h4>Harap melakukan Login terlebih dulu untuk dapat melihat protokol kesehatan secara lengkap.<a href="' + '/autentikasi/login"' + 'class="ml-2">Login sekarang!</a></h4>' +
                                                '</div>' +
                                        '</li>' +

                                        '<li class="active">' +
                                                '<h3><span></span>Website</h3>' +
                                                '<div class="row">' +
                                                    '<h4><a href="' + website + '" target="_blank">' + website + '</a></h4>' +
                                                '</div>' +
                                        '</li>' +

                                        '<li class="active">' +
                                                '<h3><span></span>Alamat Email</h3>' +
                                                '<div class="row">' +
                                                    '<h4>' + email + '</h4>' +
                                                '</div>' +
                                        '</li>' +

                                        '<li class="active">' +
                                                '<h3><span></span>Nomor Telpon</h3>' +
                                                '<div class="row">' +
                                                    '<h4>' + telpon + '</h4>' +
                                                '</div>' +
                                        '</li>' +

                                    '</ul>' +
                                '</div>'
                            );
                        }
                        
                    }
                } else {
                    alert("❌ OOPS ❌ Data " + forms[1]['value'] + " belum tersedia atau terdapat kesalahan ejaan nama provinsi. Mohon diperiksa kembali 🙏🏻");
                }
            }
        })
    })

})