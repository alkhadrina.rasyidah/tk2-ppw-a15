from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from main.models import *
from .forms import PencarianForm
from pencarian import forms
from main import models
from django.contrib import messages
from rest_framework import serializers
from collections import OrderedDict
import requests
import json

# Create your views here.

def pencarian(request):
    if request.method == 'POST':
        form = PencarianForm(request.POST)
        if form.is_valid():
            return redirect('/pencarian/')
    else:
        form = PencarianForm()

    response = {
        'form' : form
    }
             
    return render(request, 'pencarian/pencarian.html', response)

class VerifiedInfoSerializer(serializers.ModelSerializer):
    provinsi = serializers.CharField(read_only=True, source="info_id.nama_provinsi.nama")
    protokol = serializers.CharField(read_only=True, source="info_id.protokol_kesehatan")

    class Meta:
        model = VerifiedInfo
        fields = ['provinsi','protokol']

class ProvinsiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provinsi
        fields = ['nama']

def get_all_info(request, query):
    verified = VerifiedInfo.objects.filter(info_id__nama_provinsi__nama=query).select_related('info_id')
    serialize = VerifiedInfoSerializer(verified, many=True).data
    
    for i in verified:
        website = i.info_id.website_pemda.all()[0].alamat
        email = i.info_id.email_pemda.all()[0].email
        telpon = i.info_id.telpon_pemda.all()[0].nomor_telpon
    
    for i in range(len(serialize)):
        serialize[i].update({'website' : website})
        serialize[i].update({'email' : email})
        serialize[i].update({'telpon' : telpon})

    data={
        'verified':serialize,
    }

    return JsonResponse(data)

