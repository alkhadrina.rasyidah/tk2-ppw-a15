from django import forms
from main.models import Provinsi

class PencarianForm(forms.ModelForm):
    class Meta:
        model = Provinsi
        fields = ['nama']
        widgets = {
            'nama':forms.TextInput(attrs={
                'class':'form-control',
                'id' : 'query',
                'placeholder' : 'Masukkan nama provinsi',
                'oninvalid' : "alert('Masukkan nama provinsi yang ingin dicari terlebih dahulu!')",
            })
        }
