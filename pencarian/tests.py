from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from . import views, forms
from main.models import *
from .apps import PencarianConfig
from .views import pencarian
from .forms import PencarianForm
from datetime import datetime, date
import http
from http import HTTPStatus
from django.contrib.auth import login, authenticate, logout

# Create your tests here.
class PencarianTest(TestCase):
    # test urls
    def test_url_get_pencarian(self):
        response = Client().get('/pencarian/')
        self.assertEqual(response.status_code, 200)

    # test views
    def test_func_pencarian(self):
        found = resolve('/pencarian/')
        self.assertEqual(found.func, views.pencarian)

    def test_templates_pencarian(self):
        response = Client().get("/pencarian/")
        self.assertTemplateUsed(response, 'pencarian/pencarian.html')

    def test_text_pencarian(self):
        response = Client().get("/pencarian/")
        html_response = response.content.decode('utf8')
        self.assertIn("Pencarian", html_response)
        self.assertIn("Nama Provinsi", html_response)
        self.assertIn("Search", html_response)
        self.assertIn("Masukkan nama provinsi", html_response)

    def test_json_pencarian(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        obj2 = Provinsi.objects.create(nama='Jawa Barat',nama_pulau=obj1)
        obj3 = Info.objects.create(tanggal=datetime.today(),nama_provinsi=obj2,protokol_kesehatan="pakai masker")
        obj4 = WebsitePemda.objects.create(info_id=obj3,alamat="https://jawabarat.go.id/")
        obj5 = EmailPemda.objects.create(info_id=obj3,email="jawabarat@go.id")
        obj6 = TelponPemda.objects.create(info_id=obj3,nomor_telpon="08123456789")
        obj7 = VerifiedInfo.objects.create(info_id=obj3)
        response = Client().get('/pencarian/getInfo/Jawa%20Barat')
        count_verified = VerifiedInfo.objects.all().count()
        count_provinsi = Provinsi.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, count_verified)
        self.assertEqual(1, count_provinsi)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(PencarianConfig.name, 'pencarian')
		self.assertEqual(apps.get_app_config('pencarian').name, 'pencarian')