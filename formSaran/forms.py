from django import forms

from main.models import Saran

class PostForm(forms.ModelForm):
    class Meta:
        model = Saran
        fields =[
            'email',
            'jenis_saran',
            'saran',
   
            
        ]

        widgets = {
            'email': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'isi dengan email anda',
                }    
            ),
            'jenis_saran': forms.Select(
                attrs = {
                    'class': 'form-control',
                    'placeholder' : 'isi keterangannya',
                }
            ),
            'saran': forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    
                }
            ),


        }