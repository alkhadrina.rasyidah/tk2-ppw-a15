from django.urls import path

from . import views

app_name = 'formSaran'

urlpatterns = [
    path('create/', views.create, name='create'),
    path('show/', views.tampilkan_data, name='show'),
    
]