from main.models import Saran
from django.shortcuts import render,redirect
from django.http import JsonResponse
from rest_framework import serializers
from .forms import PostForm


from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
import json
from django.shortcuts import *
from django.template import RequestContext

# Create your views here.

def create(request):
    posts = Saran.objects.all()
    post_form = PostForm(request.POST or None)

    if request.method == 'POST' :#post request dari browser
        if post_form.is_valid():
            post_form.save()
            post_form = PostForm()
            return redirect('formSaran:create')
    context = {
        'page_title':'Saran',
        'post_form': post_form,
        'posts':posts,
    }
    return render(request,'formSaran/create.html',context)

def tampilkan_data(request):
    listSaran = Saran.objects.all()
    saranSerialized = SaranSerializer(listSaran, many = True).data
    data = {
            'saran': saranSerialized,
            
        }
    return JsonResponse(data)

class SaranSerializer(serializers.ModelSerializer):
	class Meta:
		model = Saran
		fields = [
            'email',
            'jenis_saran', 
            'saran', 
        ]
